# DATA ATTRIBUTION

The file `manhattan.tif` is a [Digital Orthophoto Quadrangle (DOQ)](https://lta.cr.usgs.gov/DOQs) from the USGS and in the public domain ([direct link](http://earthexplorer.usgs.gov/metadata/1131/DI00000000845784)). They [request](https://lta.cr.usgs.gov/citation) the following attribution statement when using thir data: "Data available from the U.S. Geological Survey."

The file `nybb.shp` taken from: [SciPy-Tutorial-2015](https://github.com/kjordahl/SciPy-Tutorial-2015/tree/master/examples/nybb_15b)
