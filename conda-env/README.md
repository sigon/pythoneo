# CONDA ENV

Recreate this environment using the environment.yml file and conda:

```
$ conda env create -f environment.yml
$ conda activate rasterio-env
$ conda env export > environment.yml
```
