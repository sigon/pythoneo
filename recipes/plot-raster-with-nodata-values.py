#!/usr/bin/python

# attributes:
# https://www.earthdatascience.org/courses/earth-analytics-python/lidar-raster-data/open-lidar-raster-python/
# https://stackoverflow.com/questions/2643953/attributeerror-while-adding-colorbar-in-matplotlib/11558276

import rasterio
import os
import numpy as np
from matplotlib import colors
from matplotlib import pyplot
from rasterio.plot import show_hist
from rasterio.plot import show

# get current working directory (path/to/pythoneo/recipes):
cwd = os.getcwd()
# change dir to parent folder (path/to/pythoneo)
os.chdir(os.path.abspath(os.path.join(cwd, os.pardir)))

with rasterio.open("data/dem/el.mdt_cyl_mde_025_bu_clip_4326.tif") as src:
    # read band 1 from the DEM raster image
    dem_im = src.read(1)

    # Create a spatial extent object using rasterio.plot.plotting
    spatial_extent = rasterio.plot.plotting_extent(src)

    # Reassign all 0 values to masked (no data value)
    # This will prevent pixels equal to 0 from being rendered on a map in matplotlib
    dem_im_ma = np.ma.masked_where(dem_im == 0,
                            dem_im,
                            copy=True)

    print("DEM image masked 0 values, min value:", dem_im_ma.min())
    print("DEM image masked 0 values, max value:", dem_im_ma.max())

    fig, ax = pyplot.subplots(figsize = (10,6))

    ax.set_title("raster DEM with masked 0 values", fontsize= 20)
    chm_plot = ax.imshow(dem_im_ma,
              cmap='terrain',
              extent=spatial_extent)

    # add legend
    colorbar = fig.colorbar(chm_plot, ax=ax)

    pyplot.show()
