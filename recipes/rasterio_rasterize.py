"""
Using GeoPandas to generate a vector mask and applying it to a raster image.

Kelsey Jordahl
SciPy tutorial 2015

** modified by sigon 2019

"""

import rasterio
from rasterio.features import rasterize
from geopandas import read_file
import os
import matplotlib.pyplot as plt
from constants import DATA_PATH

vector_file = DATA_PATH + 'NY/test.shp'
raster_file = DATA_PATH + 'NY/manhattan.tif'

df = read_file(vector_file)
# extract the geometry in GeoJSON format
geoms = df.geometry.values # list of shapely geometries
geometry0 = geoms[0] # shapely geometry

def show_img(img, bounds):
    left, bottom, right, top = bounds
    plt.imshow(img, cmap='gray', extent=(left, right, bottom, top))
    locs, labels = plt.xticks()
    plt.setp(labels, rotation=90)

with rasterio.open(raster_file) as src:
    # all valid ways to iterate:
    # shapes = [(x.geometry, x.id) for i, x in df.iterrows()]
    # shapes = ((x.geometry, x.id) for i, x in df.iterrows())
    # shapes = ((geom,value) for geom, value in zip(df.geometry, df.id))
    shapes = [(geom,value) for geom, value in zip(df.geometry, df.id)]

    # extract id, transform coordinates and geometry only
    mask = rasterize(
        shapes,
        transform=src.affine,
        out_shape=src.shape)

# copy raster template metadata
meta = src.meta.copy()
meta.update(nodata=0, count=1, dtype=rasterio.uint8, transform=src.affine)

# write mask as another 8-bit (rasterio.uint8) image (tif) by open in 'w' mode
with rasterio.open(
        DATA_PATH + 'NY/manhattan_mask_2.tif', 'w', # open in write mode
        **meta) as dst:
    dst.write(mask, indexes=1) # write band 1

plt.figure(1)
show_img(mask, src.bounds)
plt.savefig(DATA_PATH + 'NY/mask_2.png', dpi=300)
plt.show()

plt.figure(2)
src = rasterio.open(raster_file)
plt.imshow(src.read(1), cmap='pink')
plt.show()


# https://gis.stackexchange.com/questions/290110/python-rasterize-sum-variable
# https://gis.stackexchange.com/questions/151339/rasterize-a-shapefile-with-geopandas-or-fiona-python
# https://snorfalorpagus.net/blog/2014/11/09/masking-rasterio-layers-with-vector-features/
# https://gist.github.com/sgillies/8655640
