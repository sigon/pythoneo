from scihubcredentials import *
import os
from datetime import date
from sentinelsat import SentinelAPI


# get current working directory (path/to/pythoneo/recipes):
cwd = os.getcwd()
# change dir to parent folder (path/to/pythoneo)
os.chdir(os.path.abspath(os.path.join(cwd, os.pardir)))


api = SentinelAPI(SCIHUB_USER, SCIHUB_PASSWORD)
tiles = ['30TVN', '30TWM']
ingestion_date_FROM = date(2016, 11, 25)
ingestion_date_TO = date(2016, 12, 25)
sentinel_update_naming_convention = date(2016, 12, 6)

# NB: The tileid parameter only works for products from April 2017 onward due
# to missing metadata in SciHub’s DHuS catalogue.
# Before that, but only from December 2016 onward (i.e. for single-tile products),
# you can use a filename pattern instead:

# kw['filename'] = '*_T{}_*'.format(tile)  # products after 2016-12-01
# kw['tileid'] = tile  # products after 2017-03-31

# products = api.query(tileid= '30TVN',
#                      date = ('NOW-14DAYS', 'NOW'),
#                      platformname = 'Sentinel-2',
#                      producttype = 'S2MSI1C',
#                      cloudcoverpercentage = (0, 30))
query_kwargs = {
        'platformname': 'Sentinel-2',
        'producttype': 'S2MSI1C',
        'date': (ingestion_date_FROM, ingestion_date_TO),
        'cloudcoverpercentage': (0, 30)}

for tile in tiles:
    kw = query_kwargs.copy()
    if ingestion_date_FROM < sentinel_update_naming_convention:
        kw['filename'] = '*_T{}_*'.format(tile)
    else:
        kw['tileid'] = tile  # products after 2017-03-31
    products = api.query(**kw)

#  GeoPandas GeoDataFrame with the metadata of the scenes and the footprints as geometries
products_df = api.to_geodataframe(products)
print(products_df[['size', 'title', 'filename']])

# api.download_all(products , 'data/s2_files/')
