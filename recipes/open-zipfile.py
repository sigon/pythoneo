# create another environment
# conda create -n zipfile-env
# conda activate zipfile-env
# conda install -c conda-forge zipfile36

import os
import sys
if sys.version_info >= (3, 6):
    import zipfile
else:
    import zipfile36 as zipfile

# get current working directory (path/to/pythoneo/recipes):
cwd = os.getcwd()
# change dir to parent folder (path/to/pythoneo)
os.chdir(os.path.abspath(os.path.join(cwd, os.pardir)))



zippath = 'tmp/S2A_MSIL1C_20181103T111221_N0206_R137_T30TWM_20181103T114257.zip'
zip_ref = zipfile.ZipFile(zippath, 'r')
zip_ref.extractall(os.path.join('.', 'data/s2_files'))
zip_ref.close()
