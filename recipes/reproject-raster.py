#!/usr/bin/python

# Original source:
# Copyright (c) Earth Lab https://www.earthdatascience.org.
# https://www.earthdatascience.org/courses/earth-analytics-python/lidar-raster-data/reproject-raster/

import rasterio as rio
import os
from rasterio.warp import calculate_default_transform, reproject, Resampling


# get current working directory:
cwd = os.getcwd()
# change dir to parent folder
os.chdir(os.path.abspath(os.path.join(cwd, os.pardir)))

def reproject_et(inpath, outpath, new_crs):
    dst_crs = new_crs
    with rio.open(inpath) as src:
        transform, width, height = calculate_default_transform(
            src.crs, dst_crs, src.width, src.height, *src.bounds)
        kwargs = src.meta.copy()
        kwargs.update({
            'crs': dst_crs,
            'transform': transform,
            'width': width,
            'height': height
        })

        with rio.open(outpath, 'w', **kwargs) as dst:
            for i in range(1, src.count + 1):
                reproject(
                    source=rio.band(src, i),
                    destination=rio.band(dst, i),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=transform,
                    dst_crs=dst_crs,
                    resampling=Resampling.nearest)

reproject_et(inpath = 'data/dem/el.mdt_cyl_mde_025_bu_clip.tif',
              outpath = 'data/dem/el.mdt_cyl_mde_025_bu_clip_4326.tif',
              new_crs = 'EPSG:4326')

# Check to make sure function worked, then close raster
mdt_cyl_mde = rio.open('data/dem/el.mdt_cyl_mde_025_bu_clip_4326.tif')
print(mdt_cyl_mde.meta)
mdt_cyl_mde.close()
