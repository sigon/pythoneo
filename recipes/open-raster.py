import rasterio

raster_dataset = rasterio.open("./../data/dem/el.mdt_cyl_mde_025_bu_clip.tif")

print('rasterio type:' , type(raster_dataset))
print('raster crs:' ,raster_dataset.crs)
print('raster affine trasnsformation:' )
print(raster_dataset.affine)
print('raster width:' ,raster_dataset.width)
print('raster height:' ,raster_dataset.height)
print('raster band count:' ,raster_dataset.count)
print('raster data format:' ,raster_dataset.driver)
print('raster no data:' ,raster_dataset.nodatavals)
print('raster metadata:' ,raster_dataset.meta)

raster_dataset.close() # close connection
