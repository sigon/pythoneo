#!/usr/bin/python

import rasterio
import fiona
import os
import numpy as np

from matplotlib import pyplot
from matplotlib import colors
from rasterio.plot import show
from rasterio.plot import show_hist
from rasterio.mask import mask

# get current working directory (path/to/pythoneo/recipes)
cwd = os.getcwd()
# change current dir to parent folder (path/to/pythoneo)
os.chdir(os.path.abspath(os.path.join(cwd, os.pardir)))

# open shapefile with fiona
with fiona.open("data/POLYGON.shp") as shapefile:
    print(shapefile.driver)
    print(shapefile.crs)
    print(shapefile.bounds)

    geoms = [feature["geometry"] for feature in shapefile]

# open raster file with rasterio
with rasterio.open("data/dem/el.mdt_cyl_mde_025_bu_clip_4326.tif") as raster_data:
    print(raster_data.crs)
    out_image, out_transform = mask(raster_data, geoms, crop=True)
    # metadata for writing or exporting the data
    out_meta = raster_data.meta.copy()
    extent = rasterio.plot.plotting_extent(raster_data)

# Update the metadata to have the new shape (x and y and transform information)
out_meta.update({"driver": "GTiff",
                 "height": out_image.shape[1],
                 "width": out_image.shape[2],
                 "transform": out_transform})
print('original TIF min value:' ,out_image.min())
print('original TIF max value:' ,out_image.max())

raster_ma = np.ma.masked_where(out_image == 0 ,
                              out_image,
                              copy=True)

print('raster_ma TIF min value:' ,raster_ma.min())
print('raster_ma TIF max value:' ,raster_ma.max())

# plotlib [figsize= width, height in inches].
fig, ax = pyplot.subplots(figsize=(10,6))
ax.set_title("cliped raster, DEM -25m)\n tiff format, colormap=terrain", fontsize = 16);

# write output file in tmp folder and plot it
with rasterio.open("tmp/mde_025_bu_clip_4326_clip.tif", "w", **out_meta) as dest:
    dest.write(raster_ma)
    # plot: (raster-file, band 1), mapplolit colormap, extent
    show(
        (dest, 1),
        cmap='terrain',
        extent = extent)
