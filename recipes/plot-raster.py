import rasterio
from rasterio.plot import show
from matplotlib import pyplot

# use the context manager so it will handle opening and closing the raster file for you.
with rasterio.open("./../data/dem/el.mdt_cyl_mde_025_bu_clip.tif") as raster:

    # plotlib [figsize= width, height in inches].
    fig, ax = pyplot.subplots(figsize=(10,6))
    ax.set_title("raster DEM\n tiff 25m", fontsize = 16);

    # plot: (raster-file, band 1), mapplolit colormap
    show(
        (raster, 1),
        cmap='terrain')
