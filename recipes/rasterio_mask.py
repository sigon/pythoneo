"""
Using GeoPandas to generate a vector mask and applying it to a raster image.

Kelsey Jordahl
SciPy tutorial 2015

** modified by sigon 2019

"""

import rasterio
from rasterio.features import rasterize
from geopandas import read_file
import os
import matplotlib.pyplot as plt
from constants import DATA_PATH


vector_file = DATA_PATH + 'NY/nybb.shp'
raster_file = DATA_PATH + 'NY/manhattan.tif'

df = read_file(vector_file)
plt.figure(1)

def show_img(img, bounds):
    left, bottom, right, top = bounds
    plt.imshow(img, cmap='gray', extent=(left, right, bottom, top))
    locs, labels = plt.xticks()
    plt.setp(labels, rotation=90)

with rasterio.open(raster_file) as src:
    print(df.head())
    # extract Manhattan, transform coordinates and geometry only
    featureIndex = df[df['BoroName'] == 'Manhattan'].index[0]
    poly = df.to_crs(src.crs).iloc[featureIndex]['geometry']

    coords = [p.exterior.coords.xy for p in poly]
    ax = plt.subplot(1, 2, 1)
    img = src.read(1) #read band 1
    show_img(src.read(1), src.bounds)
    for x, y in coords:
        plt.plot(x, y, color='red', linewidth=2)
    ax.set_xlim(src.bounds.left, src.bounds.right)
    ax.set_ylim(src.bounds.bottom, src.bounds.top)
    plt.subplot(1, 2, 2)
    # rasterize([poly)] features burn value will be 1 (default)
    # rasterize([(poly, 23)] features burn value will be 23
    mask = rasterize([(poly, 23)], transform=src.affine, out_shape=src.shape)

    img[mask==0] = 255
    show_img(img, src.bounds)

# copy raster template metadata
meta = src.meta.copy()
meta.update(nodata=0, count=1, dtype=rasterio.uint8)

# write mask as another 8-bit (rasterio.uint8) image (tif) by open in 'w' mode
with rasterio.open(
        DATA_PATH + 'NY/manhattan_mask.tif', 'w', # open in write mode
        **meta) as dst:
    dst.write(mask, indexes=1) # write band 1

plt.savefig(DATA_PATH + 'NY/rasterize_mask.png', dpi=300)
plt.figure(2)
show_img(mask, src.bounds)
plt.savefig(DATA_PATH + 'NY/mask.png', dpi=300)
plt.show()
