
# https://github.com/boundlessgeo/mgrspy
from mgrspy import mgrs

latitude = 42.21
longitude = -3.42
c=mgrs.toMgrs(latitude, longitude)
# Sentinel tiles are 100km square
print(c[:5])
