# attributions:
# https://spectraldifferences.wordpress.com/2016/08/16/convert-sentinel-2-data-using-gdal/
# https://bitbucket.org/petebunting/rsgis_scripts/src/86384009f95ff235ff1f4361ad6cb41e5f746548/Sentinel/extract_s2_data.py?at=default&fileviewer=file-view-default
# https://github.com/pmlrsg/arsf_dem_scripts/blob/master/arsf_dem/get_gdal_drivers.py

import rasterio
import os
from osgeo import gdal

# print gdal drivers list:
# for i in range(gdal.GetDriverCount()):
#     drv = gdal.GetDriver(i)
#     if drv.GetMetadataItem(gdal.DCAP_RASTER):
#         print(drv.GetMetadataItem(gdal.DMD_LONGNAME), drv.GetMetadataItem(gdal.DMD_EXTENSIONS))

# get current working directory (path/to/pythoneo/recipes):
cwd = os.getcwd()
# change dir to parent folder (path/to/pythoneo)
os.chdir(os.path.abspath(os.path.join(cwd, os.pardir)))


# Try to import NERC-ARF 'get_gdal_drivers' library
# Available from https://github.com/pmlrsg/arsf_dem_scripts/blob/master/arsf_dem/get_gdal_drivers.py
HAVE_GET_GDAL_DRIVERS = False
try:
    import get_gdal_drivers
    HAVE_GET_GDAL_DRIVERS = True
except ImportError:
    pass


SLC_title = 'S2A_MSIL1C_20181103T111221_N0206_R137_T30TWM_20181103T114257'
## open the file with rasterio
# data/s2_files/S2A_MSIL1C_20181103T111221_N0206_R137_T30TWM_20181103T114257.SAFE/manifest.safe
SLC_fi_safe = os.path.join('data/s2_files', SLC_title + '.SAFE', 'manifest.safe')
SLC_fi_xml = os.path.join('data/s2_files', SLC_title + '.SAFE', 'MTD_MSIL1C.xml')

SLC_fi_B01 = os.path.join('data/s2_files', SLC_title + '.SAFE', '/GRANULE/L1C_T30TWM_A017580_20181103T111436/IMG_DATA//T30TWM_20181103T111221_B01.jp2' )
PATH= 'data/s2_files/S2A_MSIL1C_20181103T111221_N0206_R137_T30TWM_20181103T114257.SAFE/GRANULE/L1C_T30TWM_A017580_20181103T111436/IMG_DATA/T30TWM_20181103T111221_B01.jp2'
print('____ reading:')
print(SLC_fi_xml)
dataset = gdal.Open(SLC_fi_xml)
subdatasets = dataset.GetSubDatasets()
dataset = None
resolution=10
VALID_RESOLUTIONS = (10,20,60)
out_names = []
print('____  available subdatasets:')
print(subdatasets)
print('____  10m resolution subdataset:')

# Last two subdatasets are preview images so ignore these.
for sub_data in subdatasets[:-2]:
    if resolution is not None:
        if resolution not in VALID_RESOLUTIONS:
            raise Exception("Resolution {} is not valid. Available options "
                            "are 10, 20 and 60".format(resolution))
        # Get resolution
        subdata_res = sub_data[0].split(":")[2].rstrip("m")

        if int(subdata_res) == int(resolution):
            out_names.append(sub_data)
    else:
        out_names.append(sub_data)
print(out_names)
