#!/usr/bin/python

# attribution:
# https://krstn.eu/download-Sentinel-2-images/
# https://github.com/earthlab/sentinel_api/blob/master/using_sentinel_sat_api.ipynb
# https://sentinelsat.readthedocs.io/en/stable/api.html#id2
# https://sentinelsat.readthedocs.io/en/stable/api.html?highlight=download
# https://pypi.org/project/sentinelsat/
# https://pythonhow.com/accessing-dataframe-columns-rows-and-cells/
# https://automating-gis-processes.github.io/2016/Lesson2-geopandas-basics.html

import os
from sentinelsat.sentinel import SentinelAPI
from sentinelsat import read_geojson, geojson_to_wkt
from scihubcredentials import *
import matplotlib.pyplot as plt
import requests
import matplotlib.image as mpimg
import geopandas

# get current working directory (path/to/pythoneo/recipes):
cwd = os.getcwd()
# change dir to parent folder (path/to/pythoneo)
os.chdir(os.path.abspath(os.path.join(cwd, os.pardir)))

# search criteria:
geojson_footprint_path = 'data/sierra-demanda.geojson'
ingestion_date_FROM = '20181102'
ingestion_date_TO = '20181104'
platform = 'Sentinel-2' # ['1', '2', '3']
producttype = 'S2MSI2A' # ['S2MSI2A', 'S2MSI1C']only one is necessary, platform, producttype, or mission
max_cloud_percentage = 30

# set connection
api = SentinelAPI(
    user= SCIHUB_USER,
    password= SCIHUB_PASSWORD,
    api_url="https://scihub.copernicus.eu/apihub/"
)

# 'date': ('NOW-14DAYS', 'NOW')}
# date = ('20181102', date(2018, 11, 4)),
# producttype='SLC',
# orbitdirection='ASCENDING'
# sentinel=[None, '1', '2', '3'][SENTINEL],
# instrument=[None, 'MSI', 'SAR-C SAR', 'SLSTR', 'OLCI', 'SRAL'][INSTRUMENT],
# producttype=[None, 'SLC', 'GRD', 'OCN', 'RAW', 'S2MSI1C', 'S2MSI2Ap'][PRODUCTTYPE],

# Query products in our AOI.
# query(area=None,date=None,raw=None,area_relation=’Intersects’,order_by=None,
# limit=None,offset=0,**keywords)
# date= expects a tuple of (start, end)
# area_relation({'Intersects', 'Contains', 'IsWithin'},optional)
# – What relation to use for testing the AOI. Case insensitive.
# –Intersects: true if the AOI and the footprint intersect (default)–
# Contains: true if the AOI is inside the footprint
# –IsWithin: true if the footprint is inside the AOI

footprint = geojson_to_wkt(read_geojson(geojson_footprint_path))
products_S2 = api.query(footprint,
                     date = (ingestion_date_FROM, ingestion_date_TO),
                     platformname = 'Sentinel-2',
                     producttype = 'S2MSI2A',
                     cloudcoverpercentage = (0, max_cloud_percentage))
print('-------------------------------------------------')

# products in a python dictionary
s2_items = list(products_S2.items())
print('number of products:', len(s2_items))
#  GeoPandas GeoDataFrame with the metadata of the scenes and the footprints as geometries
s2_res_df = api.to_geodataframe(products_S2)
print(list(s2_res_df))
# world = geopandas.read_file('data/naturalearth_lowres/naturalearth_lowres.shp')
gp_footprint = geopandas.read_file(geojson_footprint_path)
# s2_res_df = s2_res_df.to_crs(world.crs)
gp_footprint = gp_footprint.to_crs(s2_res_df.crs)
base = gp_footprint.plot(color='white', edgecolor='black')
# base = gp_footprint.loc[[0],'geometry'].plot(color='white', edgecolor='black')

s2_res_df.plot(ax=base, color='red', alpha=0.5)

print('')
print('Sentinel-2 results:')
# print(s2_res_df.head()) # prints the first 5 rows by default
# print(s2_res_df.describe()['cloudcoverpercentage'])
print('Sentinel-2 results / producttype:')
print(s2_res_df['uuid'])
print(s2_res_df[['size', 'title', 'geometry']])
s2_res_df_sorted = s2_res_df.sort_values(['cloudcoverpercentage', 'ingestiondate'], ascending=[True, True])
## subset Sentinel-2 results by 'producttype'
prod_groups_list = list(s2_res_df.groupby('producttype'))
print('ippppppppppppppppppppo-------------------------------------------------')
print(s2_res_df.geometry.name)
print('io-------------------------------------------------')
for i, item in enumerate(prod_groups_list):
    print('producttype: ', item[0])
print('io-------------------------------------------------')

# download a couple scenes by uuid
# help(api.download)
L1C_uuids = [] # all uuids in dataset



L1C_uuid = s2_res_df['uuid'][0]
L1C_title1 = s2_res_df['title'][0]
L1C_title6 = s2_res_df['title'][1]
L1C_title7 = s2_res_df['title'][2]
print(L1C_uuid)
print(L1C_title7)
# todo
# loop through all quicklook images and then ask the user to select which one to download
print('Downloading l1c product')
# L1Cdl = api.download(L1C_uuid, 'data/s2_files/')


def download_quicklook(id_, title):
    url = "https://scihub.copernicus.eu/apihub/odata/v1/Products('{}')/Products('Quicklook')/$value".format(id_)
    bytes_img = requests.session().get(url, auth=(SCIHUB_USER,SCIHUB_PASSWORD)).content
    # ive noticed that for some ID's theres no quicklook available
    if len(bytes_img) < 500:
           raise ValueError('No valid quicklook found')
    if not os.path.exists('data/s2_files'):
        os.makedirs('data/s2_files')
    write_image = open('data/s2_files/'+ title +'_quicklook.jpg', 'wb')
    write_image.write(bytes_img)
    write_image.close()

#  download quicklook images for all files in dataset
quicklook_im_urls = []
i = 0
for index, row in s2_res_df.iterrows():
    i+=1
    print('Downloading title:', row['title'])
    download_quicklook(row['uuid'],row['title'])
    quicklook_im_urls.append({
                        'path':'data/s2_files/'+row['title']+'_quicklook.jpg',
                        'uuid':row['uuid'],
                        'title':row['title'],
                        'figure':'figure'+str(i)
                        })

for ql_uuid in quicklook_im_urls:
    print(ql_uuid['path'])
    fig, ax = plt.subplots(figsize=(10,6)) # Creates figure fig and add an axes, ax
    ax.set_title(ql_uuid['title'], fontsize = 16);
    image = mpimg.imread(ql_uuid['path'])
    plt.imshow(image)
    plt.show()

# odata = api.get_product_odata('9adbcc14-9764-403c-9406-e5bd72613973')
# print(odata)

# download all results from the search
# api.download_all(products)

# GeoJSON FeatureCollection containing footprints and metadata of the scenes
# api.to_geojson(products)
#
# # GeoPandas GeoDataFrame with the metadata of the scenes and the footprints as geometries
# api.to_geodataframe(products)
#
# # Get basic information about the product: its title, file size, MD5 sum, date, footprint and
# # its download url
# api.get_product_odata(<product_id>)

# Get the product's full metadata available on the server
# api.get_product_odata(<product_id>, full=True)









# filename1='S2A_MSIL2A_20181103T111221_N0209_R137_T30TVM_20181103T122857'
# filename2='S2A_MSIL2A_20181103T111221_N0209_R137_T30TWM_20181103T122857'
#
# https://scihub.copernicus.eu/dhus/odata/v1/Products('9adbcc14-9764-403c-9406-e5bd72613973')/$value
# https://scihub.copernicus.eu/dhus/odata/v1/Products('568336d6-1562-40da-8c4c-3527045e0b83')/$value
