#!/bin/bash
# scripts/gdal_gdb2shp.sh
# Takes a ZIP esri geodatabase (.gdb) and convert it to shape file
# USAGE
# ogr2ogr -f "ESRI Shapefile" dst_datasource_name src_datasource_name

# output file format name: -f "ESRI Shapefile"
# -overwrite:    Delete the output layer and recreate it empty


# create a eu-hydro-duero.shp folder with all the geometries from the geoDB
ogr2ogr -f "ESRI Shapefile" "eu-hydro-duero.shp" "Duero.gdb"
