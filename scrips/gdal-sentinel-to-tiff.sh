#!/bin/bash
# scripts/gdal-sentinel-to-tiff.sh
# Takes a ZIP .SAFE FILE (SENTINEL2 DATA) and extract files as tiff
# USAGE:./gdal-sentinel-to-tiff.sh <Input File>
# example
# $ chmod +x gdal-sentinel-to-tiff.sh
# $ ./gdal-sentinel-to-tiff.sh S2A_MSIL1C_20181103T111221_N0206_R137_T30TWM_20181103T114257


S2TITLE=$1
S2_FOLDER=/home/sigon/Proyects/pythoneo/data/s2_files/
ZIP_FOLDER=/home/sigon/Proyects/pythoneo/
S2MSI1C='.SAFE/MTD_MSIL1C.xml'
ZIP_EXTENSION='.zip'
SENTINEL2_L1C='SENTINEL2_L1C:'
S2MSI1C_DATASETS=[1,2,3,4,all]
TIF_FILE_EXTENSION='.tif'
UNDERSCORE='_'
# https://gdal.org/frmt_sentinel2.html


gdalinfo $ZIP_FOLDER$S2TITLE$ZIP_EXTENSION
echo '..................................................'
# Opening the .zip file and get available datasets:
echo Available datasets:
gdalinfo $ZIP_FOLDER$S2TITLE$ZIP_EXTENSION | grep 'SUBDATASET_._DESC='

echo -n "Select dataset (1/2/3): "
read dataset

case $dataset in
   "1") echo "dataset $dataset";;
   "2") echo "dataset $dataset";;
   "3") echo "dataset $dataset";;
   *) echo "Sorry, There is no resolution $resolution dataset";;
esac

selected_subdataset=$(gdalinfo $ZIP_FOLDER$S2TITLE$ZIP_EXTENSION | grep 'SUBDATASET_'$dataset'_NAME=' | cut -f2- -d=)
resolution=$(gdalinfo $ZIP_FOLDER$S2TITLE$ZIP_EXTENSION | grep 'SUBDATASET_'$dataset'_NAME=' |  cut -f3 -d:)

echo "Selected resolution: ${resolution}"

# generate a tif file with the selected subdataset:
echo Generating file: $S2_FOLDER$S2TITLE$dataset$UNDERSCORE$resolution$TIF_FILE_EXTENSION

tif_output=$S2_FOLDER$S2TITLE$dataset$UNDERSCORE$resolution$TIF_FILE_EXTENSION
gdal_translate $selected_subdataset \
              $tif_output \
              -co TILED=YES --config GDAL_CACHEMAX 1000 --config GDAL_NUM_THREADS 2


#  --config GDAL_NUM_THREADS 2
#  option to parallelize the processing.
#  The value to specify is the number of worker threads,
#  or ALL_CPUS to use all the cores/CPUs of the computer.

# --config GDAL_CACHEMAX 1000
# It controls the size of the GDAL block cache, in megabytes.
# It can be set in the environment on unix (bash/bourne) shell like this

# TILED=YES/NO.
# Whether the pixel data should be tiled. Default is NO (stripped tiff files)
